$( document ).ready(function() {
    $('#selectId').change(function(){
        var x= $(this).val();
        if(x=='ms'){
            $('.tableBox').css('display','block');
        }
        else {
            $('.tableBox').css('display','none');
        }
    });
});

$( document ).ready(function() {
    $('.pluseProfile').click(function(){
        var t = $(this).text();
        if(t !='Empty'){
            $(this).prev('.minusProfile').html('<i class="fa fa-minus" aria-hidden="true"></i> '+ t);
            $(this).empty();
        }
    });
    $('.minusProfile').click(function(){
        var t = $(this).text();
        if(t !='Empty'){
            $(this).next('.pluseProfile').html('<i class="fa fa-plus" aria-hidden="true"></i> '+ t);
            $(this).html('Empty');
        }
    });
});