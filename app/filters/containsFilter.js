App.filter('contains', function () {
    return function (array, key, id) {
        for(var i=0; i < array.length; i++) {
            if(array[i][key] === id){
                return true;
            }
        }
        return false;
    };
});