App.filter('dateRange', function () {
    return function (input, startDate, endDate, field) {

        var retArray = [];

        angular.forEach(input, function (obj) {
            var sd = startDate.split('/');
            var newsd = new Date(sd[2], sd[1] - 1, sd[0]);
            var ed = endDate.split('/');
            var newed = new Date(ed[2], ed[1] - 1, ed[0]);
            newed.setDate(newed.getDate() + 1);
            var receivedDate = obj[field];

            if (receivedDate >= newsd && receivedDate <= newed) {
                retArray.push(obj);
            }
        });

        return retArray;
    };
});