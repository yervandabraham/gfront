App.directive('uinfo', function(){

  return {
    // restrict to an element (A = attribute, C = class, M = comment)
    // or any combination like 'EACM' or 'EC'
    restrict: 'E',
    replace: true,
    scope: {
      info: '=info',
      roles: '=roles',
      logout: '&onlogout'
    },   
    templateUrl: 'app/templates/directives/uinfo.html'
  };
});