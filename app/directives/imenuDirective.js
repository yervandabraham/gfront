App.directive('imenu', function(){
  return {
    // restrict to an element (A = attribute, C = class, M = comment)
    // or any combination like 'EACM' or 'EC'
    restrict: 'E',
    replace: true,
    scope: {
      info: '=info'
    },   
    templateUrl: 'app/templates/directives/imenu.html'
  };
});