App.directive('panel', function () {
    return {
        // restrict to an element (A = attribute, C = class, M = comment)
        // or any combination like 'EACM' or 'EC'
        restrict: 'E',
        replace: true,
        transclude: true,
        scope: {
            panelclass: '@panelclass',
            panelfa: '@panelfa',
            panelhead: '@panelhead',
            panelbody: '=panelbody'
        },
        templateUrl: 'app/templates/directives/panel.html'
    }
});