App.directive('uattr', function () {

    return {
        // restrict to an element (A = attribute, C = class, M = comment)
        // or any combination like 'EACM' or 'EC'
        restrict: 'E',
        replace: true,
        transclude: true,
        scope: {
            info: '=info',
            attr: '=attr',
            edit: '&edit',
            save: '&save',
            cancel: '&cancel'
        },
        templateUrl: 'app/templates/directives/uattr.html'
    };
});