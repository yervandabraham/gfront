App.directive('pagination', function () {
    return {
        // restrict to an element (A = attribute, C = class, M = comment)
        // or any combination like 'EACM' or 'EC'
        restrict: 'E',
        replace: true,
        transclude: true,
        scope: {
            obj: '=obj',
            pagenum: '=pagenum',
            pagesize: '=pagesize',
            pagesizes: '=pagesizes',
            lbl: '@lbl',
            updateobj: '&updateobj'
        },
        templateUrl: 'app/templates/directives/pagination.html',
        link: function (scope, elm, attrs) {
            scope.updater = function (num) {
                scope.pagenum = Math.max(1, Math.min(num, scope.obj.totalPages));
                scope.$parent.pageNumber = scope.pagenum;
                scope.$parent.pageSize = scope.pagesize;
                scope.updateobj();
            };
        }
    }
});