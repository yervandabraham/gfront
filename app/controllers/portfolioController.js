App.controller('portfolioController', [
    '$scope', '$rootScope', '$routeParams', '$localStorage', '$http', '$filter', '$window', '$location', 'rest',
    function ($scope, $rootScope, $routeParams, $localStorage, $http, $filter, $window, $location, rest) {
        $scope.iportfolio = {};
        $scope.textLimit = 80;
        $scope.portfolio_clear = function () {
            $scope.add_portfolio = {
                title: '',
                link: '',
                text: ''
            };
        };
        $scope.show_spinner();
        $scope.define_filters();
        $scope.portfolio_clear();

        $scope.pageSizes = $scope.get_page_sizes([10,20,30,50,100], "portfolio");
        $scope.pageSize = $scope.pageSizes[3];
        $scope.pageNumber = 1;
        $scope.clear_menu();
        var menu_index = {
            editor: 1
        };
        $scope.menu[menu_index[$scope.userinfo.role]].active = "menu_active";

        $scope.updatePortfolios = function () {
            console.log($scope.pageNumber);
            $scope.show_spinner();
            $http.get(rest.rest_url + '/portfolio.php?page=' + ($scope.pageNumber - 1)
                + '&size=' + $scope.pageSize.value
                + '&title=' + $scope.titleFilter
            )
                .success(function (data, status) {
                    $scope.iportfolio = data;
                    $scope.hide_spinner(1, 1);
                    console.log(data);
                    console.log('status = ' + status);
                })
                .error(function (data, status) {
                    console.log('error on portfolio');
                    console.log(data);
                    console.log('status = ' + status);
                });
        };
        $scope.updatePortfolios();

        $scope.updatePageNumber = function (num) {
            $scope.pageNumber = Math.max(1,Math.min(num, $scope.iportfolio.totalPages));
            $scope.updatePortfolios();
        };

        $scope.portfolio_add = function () {
            console.log($scope.add_portfolio);
            var p ={
                title: $scope.add_portfolio.title,
                link: $scope.add_portfolio.link,
                text: $scope.add_portfolio.text
            }
            console.log(p);
            $scope.show_loader();
            $http.post(rest.rest_url + '/portfolio', p)
                .success(function (data, status) {
                    $('.modal').modal('hide');
                    swal("Success!", "Your portfolio was added successfully.", "success");
                    $scope.portfolio_clear();
                    $scope.hide_loader();
                    $scope.updatePortfolios();
                    console.log(data);
                    console.log('status = ' + status);
                })
                .error(function (data, status) {
                    console.log('error when posting portfolio');
                    toastr.error("Your portfolio has been failed.");
                    console.log(data);
                    console.log(status);
                });
        };

        $scope.delete_portfolio = function (id) {
            console.log(id);
            swal({
                title: "Are you sure to delete this portfolio?",
                text: "You will not be able to recover this portfolio!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function () {
                $scope.show_spinner();
                $http.delete(rest.rest_url + '/portfolio/' + id)
                    .success(function (data, status) {
                        swal("Deleted!", data.message, "success");
                        $scope.updatePortfolios();
                        console.log(data);
                        console.log('status = ' + status);
                    })
                    .error(function (data, status) {
                        console.log('error when deleting portfolio for specific id.');
                        console.log(data);
                        console.log('status = ' + status);
                    });
            });
        };
        $scope.save_portfolio = function (id) {
            var portfolio = $filter('filter')($scope.iportfolio.content, {id: id}, true)[0];
            console.log(portfolio);
            var p = {
                id: id,
                title: portfolio.title,
                link: portfolio.link,
                text: portfolio.text
            };
            console.log(p);
            $scope.show_loader();
            $http.put(rest.rest_url + '/portfolio', p)
                .success(function (data, status) {
                    toastr.success("Your portfolio was saved successfully.");
                    $('.modal').modal('hide');
                    $scope.hide_loader();
                    $scope.updatePortfolios();
                    console.log(data);
                    console.log('status = ' + status);
                })
                .error(function (data, status) {
                    console.log('error on portfolio edit');
                    toastr.error("Your portfolio has been failed.");
                    console.log(data);
                    console.log(status);
                });
        };
    }
]);