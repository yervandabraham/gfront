App.controller('userController', [
    '$scope', '$http', '$routeParams', '$location', 'rest',
    function ($scope, $http, $routeParams, $location, rest) {
        $scope.userId = $routeParams.id;
        $scope.clear_menu();
        $scope.menu[1].active = 'menu_active';
        $scope.hide_spinner(1, 1);
        $scope.new_user = {
            firstName: '',
            lastName: '',
            username: '',
            email: '',
            password: '',
            role: ''
        };
        $scope.new_user_obj = {};
        $scope.add_user = function () {
            console.log($scope.new_user);
            $scope.new_user.role = $scope.new_user_obj.role.key;
            console.log($scope.new_user);
            $http.post(rest.rest_url + '/user', $scope.new_user)
                    .success(function (data, status) {
                        console.log(data);
                        console.log('status = ' + status);
                        toastr.info(data.message);
                        $location.path('/users');
                    })
                    .error(function (data, status) {
                        console.log('error on user/add');
                        console.log(data);
                        console.log('status = ' + status);
                        toastr.error("User was not created.");
                    });
        };

        $scope.all_users = [];

        $http.get(rest.rest_url + '/user')
                .success(function (data, status) {
                    $scope.all_users = data;
                    console.log(data);
                    console.log('status = ' + status);
                })
                .error(function (data, status) {
                    console.log('error on user/list');
                    console.log(data);
                    console.log('status = ' + status);
                });

    }
]);