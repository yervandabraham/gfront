App.controller('mainController', ['$rootScope', '$scope', '$http', '$window', '$location', '$localStorage', '$filter', '$interval', '$timeout', 'Auth', 'rest',
    function ($rootScope, $scope, $http, $window, $location, $localStorage, $filter, $interval, $timeout, Auth, rest) {
        $scope.current_url = $location.path();
        console.log($scope.current_url);
        console.log($localStorage);
        $scope.now = new Date();
        $scope.spinner1 = false;
        $scope.loader = false;
        $scope.menu = [];
        $scope.add_password = {new1: '', new2: ''};
        $scope.shake = '';

        $scope.define_filters = function () {
            $scope.titleFilter = '';
        };

        function successAuth(res) {
            console.log(res);
            if (res.token) {
                console.log(res);
                $localStorage.token = res.token;
                $localStorage.user = res.username;
                $localStorage.role = res.role;

                $localStorage.success = "You've logged in successfully.";
                $scope.refresh_page();
            }
            else {
                toastr.error(res.errorMsg);
                $scope.shake = ' animated shake';
                $timeout(function () {
                    $scope.shake = '';
                }, 1000);
            }
        }

        $scope.login = function () {
            var formData = {
                username: $scope.username,
                password: $scope.password
            };

            Auth.login(formData, successAuth, function () {
                toastr.error('Invalid credentials.');
                $scope.shake = ' animated shake';
                $timeout(function () {
                    $scope.shake = '';
                }, 1000);
            });
        };

        $scope.logout = function () {
            Auth.logout(function () {
                $interval.cancel($rootScope.notificationInterval);
                toastr.warning("You've logged out successfully");
                $location.path("/login");
            });
        };

        $scope.show_spinner = function () {
            $scope.spinner1 = true;
        };
        $scope.hide_spinner = function (r, n) {
            if (r == n) {
                $scope.spinner1 = false;
            }
        };
        $scope.show_loader = function () {
            $scope.loader = true;
        };
        $scope.hide_loader = function () {
             $scope.loader = false;
        };
        $scope.refresh_page = function () {
            $window.location.reload();
        };
        $scope.get_page_sizes = function (arr, label) {
            var obj = [];
            angular.forEach(arr, function (num) {
                obj.push({value: num, label: num + ' ' + label});
            });
            return obj;
        };

        $scope.mini_navbar = function () {
            $("body").toggleClass("mini-navbar");
            SmoothlyMenu();
        };

        if ($localStorage.user) {
            console.log("after login area");
            $scope.roles = [
                {
                    id: 1,
                    key: 'editor',
                    name: 'Editor'
                }
            ];

            $scope.userinfo = {
                token: $localStorage.token,
                user: $localStorage.user,
                role: $localStorage.role,
                //roles: $scope.roles,
                edit: 'none',
                old: []
            };
            $http.get(rest.rest_url + '/user/me')
                .success(function (data, status) {
                    console.log(data);
                    console.log(status);
                    $scope.userinfo.data = data;
                })
                .error(function (data, status) {
                    console.log('error on user/me');
                });
            $scope.menu[0] = setImenu('Dashboard', 'fa-tachometer', '');
        }
        else{
            $scope.menu[0] = setImenu('Login', 'fa-signup', 'login');
        }
        $scope.menu[1] = setImenu('Gallery', 'fa-picture-o', 'portfolio');

        function setImenu(title, title_style, title_url, title_arrow, title_arrow_active, li_style, label, url, key, data) {
            return {
                title: title,
                title_style: title_style,
                title_url: title_url,
                title_arrow: title_arrow,
                title_arrow_active: title_arrow_active,
                li_style: li_style,
                label: label,
                url: url,
                key: key,
                data: data,
                active: ''
            };
        };
        $scope.clear_menu = function () {
            angular.forEach($scope.menu, function (m) {
                m.active = '';
                angular.forEach(m.data, function (current_menu) {
                    current_menu.active = '';
                });
            });
        };

        $scope.makeEditable = function (source) {
            $scope.userinfo.edit = source;
            //keep old data to use in cancel
            $scope.userinfo.old[source] = $scope.userinfo.data[source];
        };

        $scope.save = function (source) {
            var u = {
                id:  $scope.userinfo.data.id
            };
            u[source] = $scope.userinfo.data[source];
            console.log(u);
            $scope.show_loader();
            $http.put(rest.rest_url + '/user', u)
                .success(function (data, status) {
                    toastr.success("Your " + source + " was saved successfully.");
                    $scope.hide_loader();
                    console.log(data);
                    console.log('status = ' + status);
                    //$window.location.reload();
                })
                .error(function (data, status) {
                    console.log('error on user edit');
                    toastr.error("Your user data has been failed.");
                    console.log(data);
                    console.log(status);
                });

            $scope.userinfo.edit = 'none';

        };
        $scope.password_change = function () {
            if ($scope.add_password.new1 == $scope.add_password.new2) {
                $scope.userinfo.data.password = $scope.add_password.new1;
                $scope.save('password');
                $('#passwordPopup').modal('hide');
            }
            else {
                toastr.error("New passwords are mismatch");
            }
        };
        $scope.cancel = function (source) {
            $scope.userinfo.edit = 'none';
            $scope.userinfo.data[source] = $scope.userinfo.old[source];
        };

        $scope.changeLSFilter = function (name, key) {
            if (key) {
                $localStorage[name] = key;
            }
            else {
                delete $localStorage[name];
            }
            $scope.define_filters();
        }

        $scope.changeLSFilter = function (name, key) {
            if (key) {
                $localStorage[name] = key;
            }
            else {
                delete $localStorage[name];
            }
            $scope.define_filters();
        }

        $interval(function () {
            $scope.now = new Date();
        }, 1000);
    }
]);