App.run(['$rootScope', '$route', '$location', '$localStorage', '$interval', 'AclService',
    function ($rootScope, $route, $location, $localStorage, $interval, AclService) {
        $rootScope.$on("$routeChangeStart", function (event, next) {
            if ($localStorage.token == null) {
                if (next.templateUrl !== "app/templates/login.html") {
                    $location.path("/login");
                }
            }
            else {
                if (next.templateUrl == "app/templates/login.html") {
                    $location.path("/");
                }
            }

            if ($localStorage.error) {
                toastr.error($localStorage.error);
                $localStorage.error = '';
            }

            if ($localStorage.success) {
                toastr.success($localStorage.success);
                $localStorage.success = '';
            }

            if ($localStorage.warning) {
                toastr.warning($localStorage.warning);
                $localStorage.warning = '';
            }

            if ($localStorage.info) {
                toastr.info($localStorage.info);
                $localStorage.info = '';
            }

        });
        $rootScope.$on('$routeChangeSuccess', function (_, current) {
            $interval.cancel($rootScope.timerInterval);
            if (current.$$route.title) {
                $rootScope.page_title = current.$$route.title;
            }
        });
        // If the route change failed due to our "Unauthorized" error, redirect them
        $rootScope.$on('$routeChangeError', function (event, current, previous, rejection) {
            if (rejection === 'Unauthorized') {
                $location.path('/unauthorized');
            }
        });

        var aclData = {
            guest: ['login', 'view_content'],
            editor: ['logout', 'view_content', 'manage_content']
        };
        AclService.setAbilities(aclData);

        // Attach the member role to the current user
        AclService.attachRole($localStorage.role);

        $rootScope.can = AclService.can;

    }
]);