App.config(function ($routeProvider, $httpProvider) {
    $routeProvider
        .when('/',
            {
                title: 'Dashboard',
                controller: 'dashboardController',
                templateUrl: 'app/templates/dashboard.html'
            }
        )
        .when('/login',
            {
                title: 'Login',
                controller: 'mainController',
                templateUrl: 'app/templates/login.html'
            }
        )
        .when('/myprofile',
            {
                title: 'My Profile',
                controller: 'userController',
                templateUrl: 'app/templates/myprofile.html'
            }
        )
        .when('/portfolio',
            {
                title: 'Portfolio',
                controller: 'portfolioController',
                templateUrl: 'app/templates/portfolio.html',
                resolve: has_permission('view_content')
            }
        )
        .when('/unauthorized',
            {
                title: 'Permission denied',
                controller: 'mainController',
                templateUrl: 'app/templates/unauthorized.html'
            }
        )
        .otherwise({redirectTo: '/'});

    function has_permission(p) {
        return {
            'acl': ['$q', 'AclService', function ($q, AclService) {
                if (AclService.can(p)) {
                    // Has proper permissions
                    return true;
                } else {
                    // Does not have permission
                    return $q.reject('Unauthorized');
                }
            }]
        };

    }
    ;
    $httpProvider.interceptors.push(['$q', '$location', '$localStorage', function ($q, $location, $localStorage) {
        return {
            'request': function (config) {
                config.headers = config.headers || {};
                if ($localStorage.token) {
                    config.headers.Authorization = 'Bearer ' + $localStorage.token;
                }
                return config;
            },
            'responseError': function (response) {
                if (response.status === 401 || response.status === 403) {
                    delete $localStorage.token;
                    delete $localStorage.user;
                    delete $localStorage.role;
                    $location.path('/login');
                }
                return $q.reject(response);
            }
        };
    }
    ]);
});