App.factory('Auth', ['$http', '$localStorage', 'rest', function ($http, $localStorage, rest) {
        return {
            login: function (data, success, error) {
                $http({
                    url: rest.rest_url + '/user/login/index.php',
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param(data)
                }).success(success).error(error);
            },
            logout: function (success) {
                delete $localStorage.token;
                delete $localStorage.user;
                delete $localStorage.role;
                success();
            }
        };
    }
]);
